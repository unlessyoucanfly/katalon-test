import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://107.23.208.45:8128/')

WebUI.setText(findTestObject('Object Repository/changeAmountThePriceAlsoCanChange/Page_ProjectBackend/input_Username_username'), 
    'user')

WebUI.setEncryptedText(findTestObject('Object Repository/changeAmountThePriceAlsoCanChange/Page_ProjectBackend/input_Password_password'), 
    '1/VWEm4uipk=')

WebUI.click(findTestObject('Object Repository/changeAmountThePriceAlsoCanChange/Page_ProjectBackend/button_Login'))

WebUI.click(findTestObject('Object Repository/changeAmountThePriceAlsoCanChange/Page_ProjectBackend/button_add to cart'))

WebUI.click(findTestObject('Object Repository/changeAmountThePriceAlsoCanChange/Page_ProjectBackend/a_Carts            1'))

WebUI.click(findTestObject('Object Repository/changeAmountThePriceAlsoCanChange/Page_ProjectBackend/td_Garden'))

WebUI.verifyElementText(findTestObject('Object Repository/changeAmountThePriceAlsoCanChange/Page_ProjectBackend/p_Total price  20000 THB'), 
    'Total price 20000 THB')

WebUI.verifyElementText(findTestObject('Object Repository/changeAmountThePriceAlsoCanChange/Page_ProjectBackend/div_Well done You successfully added the transaction'), 
    'Well done You successfully added')

WebUI.setText(findTestObject('Object Repository/changeAmountThePriceAlsoCanChange/Page_ProjectBackend/input_Garden_amount'), 
    '3')

WebUI.click(findTestObject('Object Repository/changeAmountThePriceAlsoCanChange/Page_ProjectBackend/td_Garden'))

WebUI.verifyElementText(findTestObject('Object Repository/changeAmountThePriceAlsoCanChange/Page_ProjectBackend/p_Total price  60000 THB'), 
    'Total price 60000')

WebUI.click(findTestObject('Object Repository/changeAmountThePriceAlsoCanChange/Page_ProjectBackend/button_confirm'))

WebUI.closeBrowser()

