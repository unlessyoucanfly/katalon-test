import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFctory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://107.23.208.45:8128/')

WebUI.setText(findTestObject('Object Repository/AllTheAvailableProductsAreShown/Page_ProjectBackend/input_Username_username'), 
    'user')

WebUI.setEncryptedText(findTestObject('Object Repository/AllTheAvailableProductsAreShown/Page_ProjectBackend/input_Password_password'), 
    '1/VWEm4uipk=')

WebUI.click(findTestObject('Object Repository/AllTheAvailableProductsAreShown/Page_ProjectBackend/button_Login'))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/a_Products'), 'Products')

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/div_already added'), 'already added')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/a_Carts            1'), 'Carts 1')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/span_1'), '1')

WebDriver driver = DriverFactory.getWebDriver()

WebElement Table = driver.findElement(By.xpath('/html/body/app-root/app-cart-list/div/div/table/tbody'))

'To local rows of table it will Capture all the rows available in the table '
List<WebElement> Rows = Table.findElements(By.tagName('tr'))

println('No. of rows: ' + Rows.size())

WebUI.verifyEqual(Rows.size(), 1)

WebUI.closeBrowser()

